package com.example.preexamenjava;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import java.math.BigDecimal;
import java.math.RoundingMode;
import com.example.preexamenjava.R;
import android.app.AlertDialog;
import android.widget.Toast;
import com.example.preexamenjava.ReciboNomina;

public class reciboPago extends AppCompatActivity {

    private EditText editNumeroRecibo;
    private EditText editNombre;
    private EditText editHorasNormales;
    private EditText editHorasExtra;
    private RadioGroup radioGroupPuesto;
    private RadioButton radioAuxiliar;
    private RadioButton radioAlbanil;
    private RadioButton radioIngObra;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView textSubtotal;
    private TextView textImpuesto;
    private TextView textTotalPagar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_pago);

        editNumeroRecibo = findViewById(R.id.editNumeroRecibo);
        editNombre = findViewById(R.id.editNombre);
        editHorasNormales = findViewById(R.id.editHorasNormales);
        editHorasExtra = findViewById(R.id.editHorasExtra);
        radioGroupPuesto = findViewById(R.id.radioGroupPuesto);
        radioAuxiliar = findViewById(R.id.radioAuxiliar);
        radioAlbanil = findViewById(R.id.radioAlbanil);
        radioIngObra = findViewById(R.id.radioIngObra);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        textSubtotal = findViewById(R.id.textSubtotal);
        textImpuesto = findViewById(R.id.textImpuesto);
        textTotalPagar = findViewById(R.id.textTotalPagar);

        btnCalcular.setOnClickListener(view -> calcularNomina());

        btnLimpiar.setOnClickListener(view -> limpiarCampos());

        btnRegresar.setOnClickListener(view -> regresar());
    }

    private void calcularNomina() {
        String numeroRecibo = editNumeroRecibo.getText().toString();
        String nombre = editNombre.getText().toString();
        String horasNormales = editHorasNormales.getText().toString();
        String horasExtra = editHorasExtra.getText().toString();

        if (numeroRecibo.isEmpty() || nombre.isEmpty() || horasNormales.isEmpty() || horasExtra.isEmpty()) {
            Toast.makeText(this, "Por favor, completa todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }

        int puesto;
        switch (radioGroupPuesto.getCheckedRadioButtonId()) {
            case R.id.radioAuxiliar:
                puesto = 1;
                break;
            case R.id.radioAlbanil:
                puesto = 2;
                break;
            case R.id.radioIngObra:
                puesto = 3;
                break;
            default:
                puesto = 0;
                break;
        }

        BigDecimal subtotal = calcularSubtotal(puesto, Integer.parseInt(horasNormales), Integer.parseInt(horasExtra));
        BigDecimal impuesto = calcularImpuesto(subtotal);
        BigDecimal totalPagar = subtotal.subtract(impuesto);

        textSubtotal.setText("Subtotal: " + subtotal.setScale(2, RoundingMode.HALF_UP));
        textImpuesto.setText("Impuesto: " + impuesto.setScale(2, RoundingMode.HALF_UP));
        textTotalPagar.setText("Total a Pagar: " + totalPagar.setScale(2, RoundingMode.HALF_UP));
    }


    private BigDecimal calcularSubtotal(int puesto, int horasNormales, int horasExtra) {
        BigDecimal pagoBase = new BigDecimal(200);
        BigDecimal pagoPorHora;
        switch (puesto) {
            case 1:
                pagoPorHora = pagoBase.multiply(new BigDecimal(1.2));
                break;
            case 2:
                pagoPorHora = pagoBase.multiply(new BigDecimal(1.5));
                break;
            case 3:
                pagoPorHora = pagoBase.multiply(new BigDecimal(2));
                break;
            default:
                pagoPorHora = pagoBase;
                break;
        }

        BigDecimal subtotalNormales = pagoPorHora.multiply(new BigDecimal(horasNormales));
        BigDecimal subtotalExtra = pagoPorHora.multiply(new BigDecimal(2)).multiply(new BigDecimal(horasExtra));

        return subtotalNormales.add(subtotalExtra);
    }

    private BigDecimal calcularImpuesto(BigDecimal subtotal) {
        BigDecimal porcentajeImpuesto = new BigDecimal(0.16);
        return subtotal.multiply(porcentajeImpuesto).setScale(2, RoundingMode.HALF_UP);
    }

    private void limpiarCampos() {
        editNumeroRecibo.getText().clear();
        editNombre.getText().clear();
        editHorasNormales.getText().clear();
        editHorasExtra.getText().clear();
        radioGroupPuesto.clearCheck();
        textSubtotal.setText("Subtotal:");
        textImpuesto.setText("Impuesto:");
        textTotalPagar.setText("Total a Pagar:");
    }


    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Recibo Nomina");
        confirmar.setMessage("¿Deseas regresar?");
        confirmar.setPositiveButton("Confirmar", (dialog, which) -> finish());
        confirmar.setNegativeButton("Cancelar", null);
        confirmar.show();
    }
}
