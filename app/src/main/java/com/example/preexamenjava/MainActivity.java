package com.example.preexamenjava;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import com.example.preexamenjava.R;
import com.example.preexamenjava.MainActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnEnter;
    private Button btnExit;
    private EditText txtUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnEnter.setOnClickListener(v -> login());
        btnExit.setOnClickListener(v -> salir());
    }

    private void iniciarComponentes() {
        btnEnter = findViewById(R.id.btnEnter);
        btnExit = findViewById(R.id.btnExit);
        txtUsuario = findViewById(R.id.txtUsuario);
    }

    private void login() {
        String strUsuario = getResources().getString(R.string.usuario);

        if (txtUsuario.getText().toString().equals(strUsuario)) {
            Intent intent = new Intent(MainActivity.this, reciboPago.class);
            intent.putExtra("usuario", strUsuario);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Usuario no válido", Toast.LENGTH_LONG).show();
        }
    }

    private void salir() {
        finish();
    }
}
