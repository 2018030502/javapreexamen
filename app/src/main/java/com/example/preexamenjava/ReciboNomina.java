package com.example.preexamenjava;

public class ReciboNomina {
    private final String numeroRecibo;
    private final String nombre;
    private final double horasNormales;
    private final double horasExtra;
    private final int puesto;
    private final double impuesto;

    public static final int PUESTO_AUXILIAR = 1;
    public static final int PUESTO_ALBANIL = 2;
    public static final int PUESTO_INGENIERO_OBRA = 3;
    public static final double PAGO_BASE = 200.0;

    public ReciboNomina(String numeroRecibo, String nombre, double horasNormales, double horasExtra, int puesto) {
        this.numeroRecibo = numeroRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtra = horasExtra;
        this.puesto = puesto;
        this.impuesto = 0.16;
    }

    public ReciboNomina(String numeroRecibo, String nombre, double horasNormales, double horasExtra, int puesto, double impuesto) {
        this.numeroRecibo = numeroRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtra = horasExtra;
        this.puesto = puesto;
        this.impuesto = impuesto;
    }

    public double calcularSubtotal() {
        double pagoBase;
        switch (puesto) {
            case PUESTO_AUXILIAR:
                pagoBase = PAGO_BASE * 1.2;
                break;
            case PUESTO_ALBANIL:
                pagoBase = PAGO_BASE * 1.5;
                break;
            case PUESTO_INGENIERO_OBRA:
                pagoBase = PAGO_BASE * 2.0;
                break;
            default:
                pagoBase = PAGO_BASE;
                break;
        }

        double pagoHorasNormales = pagoBase * horasNormales;
        double pagoHorasExtra = pagoBase * horasExtra * 2;

        return pagoHorasNormales + pagoHorasExtra;
    }

    public double calcularImpuesto() {
        return calcularSubtotal() * impuesto;
    }

    public String getNumeroRecibo() {
        return numeroRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public double getHorasNormales() {
        return horasNormales;
    }

    public double getHorasExtra() {
        return horasExtra;
    }

    public int getPuesto() {
        return puesto;
    }

    public double getImpuesto() {
        return impuesto;
    }
}
